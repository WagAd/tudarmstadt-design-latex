#!/bin/sh

# this assumes the first command line parameter is a path where
# either some .zip-files already exist or 
# latex-tudesign_2016-03-01.zip
# tudfonts-tex_0.0.20090806.zip
# should be downloaded to

if ls $1/*.zip 1>/dev/null 2>&1; then
    echo "$1 contains some .zip files, skipping tu-design download."
else
    echo "$1 does not contain zip files, downloading tu-design now..."
    wget --quiet http://exp1.fkp.physik.tu-darmstadt.de/tuddesign/latex/latex-tuddesign/latex-tuddesign_1.0.20140928.zip -P $1
    wget --quiet http://exp1.fkp.physik.tu-darmstadt.de/tuddesign/latex/latex-tuddesign-thesis/latex-tuddesign-thesis_0.0.20140703.zip -P $1
fi
